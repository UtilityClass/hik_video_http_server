cmake_minimum_required(VERSION 3.0.0)
project(hv_test VERSION 0.1.0 LANGUAGES C CXX)


set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED True)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_FLAGS " -g -ggdb -Wall")

set(LIBHV_ROOT_DIR         /Users/lixiujie/dev_test/dev_cpp/libhv/build)
set(OPENSSL_ROOT_DIR       /opt/homebrew/Cellar/openssl@3/3.2.1)

link_directories(
    ${LIBHV_ROOT_DIR}/lib
    ${OPENSSL_ROOT_DIR}/lib
)
file(GLOB cpp_source_file *.cpp )
add_executable(hv_test ${cpp_source_file})

target_include_directories(hv_test
    PRIVATE .    
    PRIVATE ${LIBHV_ROOT_DIR}/include
    PRIVATE ${OPENSSL_ROOT_DIR}/include
)

target_link_libraries(hv_test libhv_static.a ssl crypto pthread dl)
